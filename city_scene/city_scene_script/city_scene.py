import bpy
import math
import mathutils
import numpy as np
import random
import itertools
from collections import namedtuple

from .helper import OredTuple


class CityScene(object):
    """ Scene setup close to a city including streets and houses """

    _O = OredTuple
    # Some constants
    DIR_RIGHT   = (1, 0)
    DIR_LEFT    = (-1, 0)
    DIR_UP      = (0, 1)
    DIR_DOWN    = (0, -1)

    TURN_LEFT       = -1
    TURN_STRAIGHT   = 0
    TURN_RIGHT      = 1

    TURN_OFFSET_LEFT        = 0
    TURN_OFFSET_STRAIGHT    = 0.18
    TURN_OFFSET_RIGHT       = 0.4
    
    ZOFFSET = -0.01
    
    maxframe = 0
    
    # Negative flags constants assigned to all textured objects but streets
    T_GRASS       = 0
    T_ARROW       = 1
    T_BUILDINGS   = (-10, -12, -13, -14, -15, -16)
    T_CARS        = (-20, )

    # Bit flgas assigned to all possible (street) directions and their combinations.
    # U=Up, D=Down, L=Left, R=Right
    # 2-Way:
    T_PTH_UD = 2**1
    T_PTH_LR = 2**2
    T_PTH_UR = 2**3
    T_PTH_UL = 2**4
    T_PTH_DR = 2**5
    T_PTH_DL = 2**6
    # 3-Way:
    T_PTH_LRU = _O(T_PTH_LR | T_PTH_UR, T_PTH_LR | T_PTH_UL, T_PTH_UR | T_PTH_UL, T_PTH_UR | T_PTH_UL | T_PTH_LR)
    T_PTH_LRD = _O(T_PTH_LR | T_PTH_DR, T_PTH_LR | T_PTH_DL, T_PTH_DR | T_PTH_DL, T_PTH_DR | T_PTH_DL | T_PTH_LR)    
    T_PTH_UDR = _O(T_PTH_UD | T_PTH_UL, T_PTH_UD | T_PTH_DL, T_PTH_UR | T_PTH_DR, T_PTH_UR | T_PTH_DR | T_PTH_UD)
    T_PTH_UDL = _O(T_PTH_UD | T_PTH_UR, T_PTH_UD | T_PTH_DR, T_PTH_UL | T_PTH_DL, T_PTH_UL | T_PTH_DL | T_PTH_UD)
    T_PTH_UDLR = _O(set(_O( 
    # 4-Way
                    T_PTH_UD | T_PTH_LR, *(T_PTH_LRU | T_PTH_LRD), 
                    *(T_PTH_LRU | T_PTH_UD), *(T_PTH_LRD | T_PTH_UD),
                    *(T_PTH_UDR | T_PTH_UDL), *(T_PTH_UDR | T_PTH_LR),
                    *(T_PTH_UDL | T_PTH_LR), 
                    T_PTH_UR | T_PTH_DL, T_PTH_UL | T_PTH_DR 
                )))
    # All possible 4-way combination with every possible path flag
    T_PTH_UDLR = _O(set(_O(
                    T_PTH_UDLR | _O(T_PTH_UD, T_PTH_LR, T_PTH_UR, T_PTH_UL, T_PTH_DR, T_PTH_DL)
                )))
    
    # Mapping from all possible direction bit flags and their combinations
    # to the corresponding texture name
    TEX_MAP = {
        T_PTH_UD      : "street_ud",
        T_PTH_LR      : "street_lr",
        T_PTH_UR      : "street_ur",
        T_PTH_UL      : "street_ul",
        T_PTH_DR      : "street_dr",
        T_PTH_DL      : "street_dl",
        
        T_PTH_LRU     : "street_lru",
        T_PTH_LRD     : "street_lrd",
        T_PTH_UDR     : "street_udr",
        T_PTH_UDL     : "street_udl",
        
        T_PTH_UDLR    : "street_udlr",
        
        T_GRASS       : "grass",
        T_ARROW       : "patharrow",
        T_BUILDINGS   : "building_",
        T_CARS        : "car_"
    }
    
    # Some TEX_MAP keys consist of multiple bit flags, let's make each its own key
    # in order to access each bit flag key directly 
    for key, val in list(TEX_MAP.items()):
        if hasattr(key, "__iter__"):
            del TEX_MAP[key]
            for k in key:
                TEX_MAP[k] = val
                
    # Enumerate TEX_MAP "building_" and "car_" values
    for obj_list in [T_BUILDINGS, T_CARS]:
        for i, obj in enumerate(obj_list, start=1):
            TEX_MAP[obj] += str(i)
                
                
    # Mapping from curvature tuple to the corresponding bit flag, calculated
    # by <prev-dir> - <dir> if result not (0,0) else <prev-dir> + <dir>
    CURVATURE_MAP = {
        (0,2)   : T_PTH_UD,
        (0,-2)  : T_PTH_UD,
        (2,0)   : T_PTH_LR,
        (-2,0)  : T_PTH_LR,
        (-1,-1) : T_PTH_UR,
        (1,-1)  : T_PTH_UL,
        (-1,1)  : T_PTH_DR,
        (1,1)   : T_PTH_DL
    }
    
    def __init__(self):
        self.size = None
        self.grid_flags = None
        self.grid_choices = None
        self.path_starts = None

    def create(self, size):
        self.delete()
        self.size = size
        self.grid_flags = np.zeros((size,size), dtype=int) # Texture flags
        self.grid_choices = np.empty((size,size), dtype=object) # Sets of possible next path points
        self.path_starts = {} # Path start points {<point> : <direction>}
        
    def delete(self):
        for obj in bpy.context.scene.objects:
            if obj.get("cs_index") is not None:
                obj.select = True
            else:
                obj.select = False
        bpy.ops.object.delete()

    def _addGridpointChoice(self, point, next_point):
        next_point = tuple(next_point)
        px, py = point
        if self.grid_choices[py][px] is None:
            self.grid_choices[py][px] = set([next_point])
        else:
            self.grid_choices[py][px].add(next_point)

    def _addPathStart(self, point, direction):
        self.path_starts[tuple(point)] = tuple(direction)

    def getFreeBorderPoints(self):
        """ Get all possible border indices not next to an existing path """
        border_points = set()
        for a, b in itertools.product([0, self.size-1], range(self.size)):
            if not any(self.getNeighbors((a, b))):
                border_points.add((a, b))
            if not any(self.getNeighbors((b, a))):
                border_points.add((b, a))
        return border_points  

    def getDirectionToCenter(self, point):
        """ Return the direction vector of a point towards the center of the grid """
        size = self.size
        direction_start = list(point)
        # If the chosen start point is a corner, we randomly perturb it for 
        # a distinct calculation of the direction (no diagonals)  
        if sum(direction_start) in (0, size-1, (size-1)*2):
            direction_start[random.choice([0,1])] += random.choice([-1, 1])
        # Direciton vector towards the center of the grid  
        direction = np.subtract(((size-1)/2, (size-1)/2), direction_start)
        # Round to one of the base direction vectors perpendicular to the border
        direction = (direction / np.abs(direction).sum()).round()
        return tuple(direction)

    @classmethod
    def getCurvature(cls, in_direction, out_direction):
            """ Return curvature from two consecutive directions """
            curvature = np.subtract(in_direction, out_direction)
            # If directions are cancled out to zero, recalculate them by addition
            if not any(curvature):
                curvature = np.add(in_direction, out_direction)
            return tuple(curvature)

    @classmethod
    def getDirection(cls, in_point, out_point):
        """ Return direction vector of two points """
        return tuple(np.subtract(out_point, in_point))

    @classmethod
    def addDirections(cls, in_direction, out_direction):
        """ Retrun the combined direction vector of two direction vectors """
        return tuple(np.add(in_direction, out_direction))

    @classmethod
    def getTurn(cls, in_direction, out_direction):
        """ Return the turn (left, straight, right) of two direction vectors """
        direction = cls.addDirections(in_direction, out_direction)
        if not all(direction):
            return cls.TURN_STRAIGHT

        curvature = cls.getCurvature(in_direction, out_direction)
        # Small calculation that equates to 5 for right and 1 or 7 for left
        lr_unique = np.multiply(direction, 2) - curvature
        lr_unique = abs(lr_unique[0]*2 - lr_unique[1])
        if lr_unique == 5:
            return cls.TURN_RIGHT
        return cls.TURN_LEFT
        
    @classmethod
    def getUniqueID(cls):
        return random.getrandbits(16)

    def addStreetPath(self, turn_filters=()):
        """ Adds a street path to the grid minding existing paths. 
            This process will populate the grid_flags array with the appropriate
            texture flags.
            This process will populate the grid_choices array with possible
            next moves at any generated path point.
            this process will populate the path_starts dictionary with the start
            and (unless ended by a joint) end point.  
        """
        size = self.size
        TWO_WAY_FLAGS = [2**n for n in range(1,7)]
        DIRECTIONS = (self.DIR_RIGHT, self.DIR_LEFT, self.DIR_UP, self.DIR_DOWN)
        
        try: # Choose a random start point
            current = random.choice(list(self.getFreeBorderPoints()))
        except IndexError: # No indices were found, all paths along the border are taken
            return False

        _direction = self.getDirectionToCenter(current)
        _turn = self.TURN_STRAIGHT
        path_data = { # Path history
            "directions"    : [_direction],
            "turns"         : [_turn],
            "indices"       : [current]       
        }
        self._addPathStart(current, _direction)            
       
        # Iteratively expand the path until stopped or a border 
        generating = True
        while generating:
            # Add previous point as a possible next point to the current point
            try:
                self._addGridpointChoice(path_data["indices"][-1], path_data["indices"][-2])
            except IndexError:
                self._addGridpointChoice(path_data["indices"][-1], 
                    tuple(np.subtract(path_data["indices"][-1], path_data["directions"][-1])))

            # Calculate the angles between all possible vectors through the arctan2 of
            # their determinants and dot products
            _prev_dir = path_data["directions"][-1]
            _scalars = np.dot(DIRECTIONS, _prev_dir)
            _dets = [np.linalg.det([dir, _prev_dir]) for dir in DIRECTIONS]
            _angles = np.arctan2(_dets, _scalars)
            # Mapping of turns to the corresponding index of the 'DIRECTIONS' array
            turns_to_dir_idx = {int(turn) : idx for idx, turn in enumerate(_angles) if turn < 2}

            # Default turn and direction probabilities and a mapping between them
            dir_probs, turn_probs, dir_turn_mapping = {}, {}, {}        
            for _angle, _direction in zip(_angles, DIRECTIONS):
                if _angle < 2:
                    _turn = int(_angle)
                    turn_probs[_turn] = dir_probs[_direction] = 1/3
                    dir_turn_mapping[_direction] = _turn
                    dir_turn_mapping[_turn] = _direction

            # Apply turn filters that manipulate the turn probabilities
            for tfilter in turn_filters:
                filter_resp = tfilter(self, path_data, turn_probs, dir_probs)
                if filter_resp is None:
                    continue
                # Align direction and turn probabilities, as either may be returned by the filter function
                if filter_resp.get("direction_probs"):
                    dir_probs = filter_resp["direction_probs"]
                    turn_probs = {dir_turn_mapping[dir] : prob for dir, prob in dir_probs.items()}
                elif filter_resp.get("turn_probs"):
                    turn_probs = filter_resp["turn_probs"]
                    dir_probs = {dir_turn_mapping[turn] : prob for turn, prob in turn_probs.items()}
                if filter_resp.get("stop") == True:
                    generating = False

            # Choose the next turn/ direction randomly
            turns, probabilities = list(turn_probs.keys()), list(turn_probs.values())
            try:
                turn = np.random.choice(turns, 1, p=probabilities)[0]
            except Exception as e:
                print("ERROR", turns, probabilities)
                print(e)
                return            
            direction = dir_turn_mapping[turn]
      
            # 'Logical or' the bit flag corresponding to that curvature with the bit flag at that grid point
            _curvature = self.getCurvature(path_data["directions"][-1], direction)      
            self.grid_flags[current[1]][current[0]] |= self.CURVATURE_MAP[_curvature]

            current = np.add(current, direction) # Get the next point

            # Add this next point as possible next point to the previous point
            self._addGridpointChoice(path_data["indices"][-1], current)

            # Check if next indices are in bound, finish if border is reached
            if not self.inBounds(current):
                break

            # Add path information of the this step to the history
            path_data["directions"].append(direction)
            path_data["turns"].append(turn)
            path_data["indices"].append(tuple(current))        

        # If path generation stopped due to out-of-bound (=end of grid),
        # add the end as a possible start
        if generating:
            self._addPathStart(path_data["indices"][-1], 
                tuple(np.multiply(path_data["directions"][-1], -1)))

    def addBuildings(self, prob_along_street=.25, prob_off_street=.25):
        """ Adds buildings randomly along and off street paths, each according 
        to the given probabiltity """
        it = np.nditer(self.grid_flags, op_flags=["readwrite"],flags=["multi_index"])
        for cell in it:
            if cell > 0: # Street
                continue
            x, y = it.multi_index
            if any(np.array(list(self.getNeighbors((y, x)))) > 0): # Along a street
                if random.random() > prob_along_street:
                    continue
            elif random.random() > prob_off_street: # Off the streets
                continue
            cell[...] = random.choice(self.T_BUILDINGS)

    def travelPath(self):
        """ Travel randomly through the street paths from border to border.
        The elements of the returned path list will consist of
        dict(
            "index"         : <index>, 
            "in-direction"  : <in-direction>, 
            "out-direction" : <out-direction>, 
            "direction"     : <direction>, 
            "curvature"     : <curvature>    
        ) 
        """
        current = random.choice(list(self.path_starts.keys())) # Choose a random start
        _prev = tuple(np.subtract(current, self.path_starts[current]))
        MAX_TRAVEL = 200 # Max travel steps in case we get stuck in a loop
        traveled = 0
        path = [] # List of {"index" :<index>, "in-direction" : <in-direction>, 
                  # "out-direction" : <out-direction>, "direction" : <direction>, 
                  # "curvature" : <curvature>} elements

        while traveled < MAX_TRAVEL:
            traveled += 1
            try:
                _prev = path[-1]["index"]
            except IndexError:
                pass
            _in_dir = self.getDirection(_prev, current)

            path.append({
                "index"         : current,
                "in-direction"  : _in_dir,
                "out-direction" : None,
                "direction"     : None,
                "curvature"     : None
            })

            # Gather valid next points
            _nexts = []
            _probs = []
            for next_point in self.grid_choices[current[1]][current[0]]:
                # Disregard the point in opposite of travel direction (180°)
                _next_dir = self.getDirection(current, next_point)
                _dot = np.dot(_in_dir, _next_dir)
                if _dot >= 0:
                    _nexts.append(next_point)
                    _probs.append(_dot+1) # prefer straight over turns

            _probs = np.divide(_probs, sum(_probs))
            _indices = range(len(_nexts))
            _index = np.random.choice(_indices, 1, p=_probs)[0]
            current = _nexts[_index]

            path[-1]["out-direction"] = self.getDirection(path[-1]["index"], current)
            path[-1]["direction"] = tuple(np.add(path[-1]["in-direction"], path[-1]["out-direction"]))
            path[-1]["curvature"] = self.getCurvature(path[-1]["in-direction"], path[-1]["out-direction"])

            if not self.inBounds(current):
                break

        return path

    def hintPath(self, path):
        """ Hint path with arrows """
        cs_index = self.getUniqueID()
        for point in path:
            dir = np.clip(point["direction"], a_min=-1, a_max=1)
            unit_dir = dir / np.linalg.norm(dir)
            rotation = np.arctan2(unit_dir[1], unit_dir[0])
            offset_point = self.getDriverSideCoord(point["index"], point["in-direction"], point["out-direction"])
            self.addObject(self.TEX_MAP[self.T_ARROW], offset_point, (0, 0, rotation), cs_index) 

        return cs_index

    def moveAlongPath(self, obj, path, frame_step=10, frame_start=0):
        """ Set keyframes for an object along to move along a path """

        for i, point in enumerate(path):
            dir = np.clip(point["direction"], a_min=-1, a_max=1)
            unit_dir = dir / np.linalg.norm(dir)
            rotation = np.arctan2(unit_dir[1], unit_dir[0])
            offset_point = self.getDriverSideCoord(point["index"], point["in-direction"], point["out-direction"])
            self.setKeyframe(obj, offset_point, (0, 0, rotation), frame_start+frame_step*i)
            
        #print(i)
           
        def recHide(obj, hide=True, frame=0):
            obj.hide_render = hide
            obj.hide = hide
            if frame >= 0:
                obj.keyframe_insert(data_path="hide_render", frame=frame, index=-1)
                obj.keyframe_insert(data_path="hide", frame=frame, index=-1)
            for child in obj.children:
                recHide(child, hide, frame)
       
        recHide(obj, hide=True)
        recHide(obj, hide=False, frame=frame_start)
        recHide(obj, hide=True, frame=frame_start+frame_step*len(path))

    def setKeyframe(self, obj, xyz, rotation, frame):
        """ Set keyframe of an object animating position and rotation """
        xyz =  list(xyz) + [0]*(3-len(xyz)) 
        xyz[0] -= self.size/2
        xyz[1] -= self.size/2
        rotation =  list(rotation) + [0]*(3-len(rotation)) 
        start_rot = obj.rotation_euler.copy()
        start_loc = obj.location.copy()
        obj.location = xyz
        obj.rotation_euler = rotation
        obj.keyframe_insert(data_path="location", frame=frame, index=-1)
        obj.keyframe_insert(data_path="rotation_euler", frame=frame, index=-1)
        obj.location = start_loc
        obj.rotation_euler = start_rot
        if frame > self.maxframe: self.maxframe = frame

    def getDriverSideCoord(self, coord, in_direction, out_direction):
        """ Get the coordinate, offset to the driver side """
        offsets = {
            self.TURN_LEFT      : self.TURN_OFFSET_LEFT,
            self.TURN_STRAIGHT  : self.TURN_OFFSET_STRAIGHT,
            self.TURN_RIGHT     : self.TURN_OFFSET_RIGHT,
        }
        turn = self.getTurn(in_direction, out_direction)
        offset = offsets[turn]
        direction = self.addDirections(in_direction, out_direction)
        offdir = (direction / np.linalg.norm(direction)) * offset
        offdir_rot90 = (offdir[1], -offdir[0])
        return tuple(np.add(coord, offdir_rot90))

    def draw(self, ignore=()):
        """ Create all objects according to texture flags within the grid array """
        it = np.nditer(self.grid_flags, flags=["multi_index"])
        for cell in it:
            x, y = it.multi_index
            cell = int(cell)
            if cell not in ignore:
                if cell == self.T_GRASS: 
                    self.addObject(self.TEX_MAP[cell], (y, x, self.ZOFFSET))
                else:
                    self.addObject(self.TEX_MAP[self.T_GRASS], (y, x, self.ZOFFSET))
                    self.addObject(self.TEX_MAP[cell], (y, x))
                
        lamp_data = bpy.data.lamps.new(name="Sun", type="SUN")
        lamp_object = bpy.data.objects.new(name="Sun", object_data = lamp_data)
        lamp_object["index"] = 0
        bpy.context.scene.objects.link(lamp_object)
        lamp_object.location = (self.size / 4, -(self.size / 4), 10)
        lamp_object.rotation_euler = (math.radians(10), math.radians(0), 0)
        lamp_data.use_nodes = True
        lamp_data.node_tree.nodes["Emission"].inputs["Strength"].default_value = 7
        lamp_data.node_tree.nodes["Emission"].inputs["Color"].default_value = (1.0, 0.9, 0.8, 1)
        #lamp_object.select = True
        
    def copyObject(self, type):
        object_ref = bpy.data.objects[type]
        if(type.startswith("car")):
            obj = self.treeCopy(object_ref, None)
        else:
            obj = object_ref.copy()
        obj["cs_index"] = 0
        return obj
    
    def treeCopy(self, obj, parent, levels = 3):
        def recurse(obj, parent, depth):
            if(depth > levels):
                return
            copy = obj.copy()
            copy["cs_index"] = 0
            copy.parent = parent
            copy.matrix_parent_inverse = obj.matrix_parent_inverse.copy()
            bpy.context.scene.objects.link(copy)
            #copy.select = True
            
            for child in obj.children:
                recurse(child, copy, depth + 1)
            return copy
            
        copy_obj = recurse(obj, obj.parent, 0)
        #copy_obj.select = True
        return copy_obj
    
    def addObject(self, type, xyz=(0,0,0), rotation=(0,0,0), cs_index=0):
        """ Create an object with a given texture at a given location """
        xyz =  list(xyz) + [0]*(3-len(xyz)) 
        xyz[0] -= self.size/2
        xyz[1] -= self.size/2
        rotation =  list(rotation) + [0]*(3-len(rotation)) 
        obj = self.copyObject(type)
        #print(rotation)
        obj.rotation_euler = [sum(x) for x in zip(obj.rotation_euler, rotation)]
        obj.location = xyz
        obj.scale = (0.5, 0.5, 0.5)
        if(type.startswith("car")): 
            obj.scale = (0.1,0.1,0.1)
        else: 
            bpy.context.scene.objects.link(obj)
        obj["cs_index"] = cs_index
        #obj.select = True
        return obj

    def removeByIndex(self, cs_index):
        for obj in bpy.context.scene.objects:
            if obj.get("cs_index") == cs_index:
                obj.select = True
            else:
                obj.select = False
        bpy.ops.object.delete()

    def getOffBoundBorderCoord(self, coord):
        offcoord = []
        for i in coord:
            p = i
            if i == 0:   p = -1
            if i == self.size-1:   p = self.size
            offcoord.append(p)
        return offcoord

    def inBounds(self, coord):
        """ Check if indices are in bound of the grid array """
        return (0 <= coord[0] < self.size and 0 <= coord[1] < self.size)
    
    def getNeighbors(self, coord, out_of_bounds_as="empty"):
        """ 
        Get grid neighbor values around the given location 
        Returns: Generator(grid values)
        """
        for a, b in itertools.product((0,-1,+1), (0,-1,+1)):
            xa, yb = coord[0]+a, coord[1]+b
            if self.inBounds((xa, yb)):
                yield self.grid_flags[yb][xa]
            elif out_of_bounds_as != "empty":
                yield out_of_bounds_as

    def projectVertexToViewport(camera, p, render = bpy.context.scene.render):
        """
        Given a camera and its projection matrix M;
        given p, a 3d point to project:

        Compute P’ = M * P
        P’= (x’, y’, z’, w')

        Ignore z'
        Normalize in:
        x’’ = x’ / w’
        y’’ = y’ / w’

        x’’ is the screen coordinate in normalised range -1 (left) +1 (right)
        y’’ is the screen coordinate in  normalised range -1 (bottom) +1 (top)

        :param camera: The camera for which we want the projection
        :param p: The 3D point to project
        :param render: The render settings associated to the scene.
        :return: The 2D projected point in normalized range [-1, 1] (left to right, bottom to top)
        """
        if camera.type != 'CAMERA':
            raise Exception("Object {} is not a camera.".format(camera.name))
            
        if len(p) != 3:
            raise Exception("Vector {} is not three-dimensional".format(p))
            
        # Get the two components to calculate M
        modelview_matrix = camera.matrix_world.inverted()
        projection_matrix = camera.calc_matrix_camera(
            render.resolution_x,
            render.resolution_y,
            render.pixel_aspect_x,
            render.pixel_aspect_y,
        )
        
        print(projection_matrix * modelview_matrix)
        
        p1 = projection_matrix * modelview_matrix * mathutils.Vector((p.x, p.y, p.z, 1))
        
        p2 = mathutils.Vector(((p1.x/p1.w, p1.y/p1.w)))
        
        return p2
        

#_city_scene = CityScene()
#
#def getCityScene():
#    return _city_scene
    

