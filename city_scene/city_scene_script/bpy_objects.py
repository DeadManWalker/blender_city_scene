import numpy as np
import random
import bpy
import bpy_extras
from bpy.props import BoolProperty
from .city_scene import CityScene
from .helper import TurnFilterCreator


global_data = {
    "city_scene" : None,
    "render_path" : None,
    "render_path_cs_index" : None,
    "cars" : []
}


class CreateCityScene(bpy.types.Operator) :
    bl_idname = "mesh.create_city_scene"
    bl_label = "Create  City Scene"
    bl_options = {"UNDO"}

    def execute(self, context):
        if global_data["city_scene"] is not None:
            global_data["city_scene"].delete()
        global_data["city_scene"] = self.scene

        self.scene.create(self.grid_size)
        
        turn_filters = (
            TurnFilterCreator.staticProbabilities(0.05, 0.9, 0.05),
            TurnFilterCreator.noConsecutiveTurns(),
            TurnFilterCreator.noTurnsAfterIntersection(),
            TurnFilterCreator.noParallelPaths(),
            TurnFilterCreator.intersectOrJoin(0.6),
            TurnFilterCreator.balanceTurns(1000)
        )
        for _ in range(self.street_count):
            self.scene.addStreetPath(turn_filters=turn_filters)
            
        self.scene.addBuildings(prob_along_street=.5, prob_off_street=.1)     
        self.scene.draw()

        return {"FINISHED"}

    def invoke(self, context, event) : 
        self.scene = CityScene()
        self.street_count = bpy.context.scene.street_count
        self.grid_size = bpy.context.scene.grid_size
        
        return self.execute(context)
        

class CreateCameraPath(bpy.types.Operator):
    bl_idname = "mesh.create_camera_path"
    bl_label = "(Re)create Camera Path"
    bl_options = {"UNDO"}

    def execute(self, context):
        if self.scene is None:
            return {"CANCELLED"}
        bpy.ops.object.camera_add(location=(0.0, 0.0, 0.0), rotation=(0.0, 0.0, 0.0))
        global_data["render_path"] = render_path = self.scene.travelPath()
        if global_data["render_path_cs_index"] is not None:
            self.scene.removeByIndex(global_data["render_path_cs_index"])
        global_data["render_path_cs_index"] = self.scene.hintPath(render_path)
        return {"FINISHED"}

    def invoke(self, context, event):   
        self.scene = global_data["city_scene"]
        return self.execute(context)


class RenderCityScene(bpy.types.Operator):
    bl_idname = "mesh.render_city_scene"
    bl_label = "Render Scene along path"
    bl_options = {"UNDO"}
    
    maxframe = 0

    def execute(self, context):
        if global_data["city_scene"] is None \
         or global_data["render_path"] is None:
            return {"CANCELLED"}

        car_speed = 10
        self.scene.removeByIndex(global_data["render_path_cs_index"])
        car = self.scene.addObject(self.scene.TEX_MAP[self.scene.T_CARS[0]], global_data["render_path"][0]["index"])
        self.scene.moveAlongPath(car, global_data["render_path"], frame_step=car_speed)
        car_path_len = len(global_data["render_path"])
        bpy.context.scene.frame_end = car_speed * car_path_len

        for i in range(self.car_count):
            total_len = 0
            while total_len < car_path_len:
                path = self.scene.travelPath()
                if i == 0:
                    p_start = random.randint(0, len(path)-1)
                    path = path[p_start:]
                car = self.scene.addObject(self.scene.TEX_MAP[self.scene.T_CARS[0]], path[0]["index"])
                self.scene.moveAlongPath(car, path, frame_start=total_len*car_speed, frame_step=car_speed)
                total_len += len(path)-1
        bpy.context.scene.frame_end = self.scene.maxframe + 10
        return {"FINISHED"}

    def invoke(self, context, event):   
        allobjs = bpy.data.objects
        for ob in allobjs:
            ob.animation_data_clear()
        self.scene = global_data["city_scene"]
        self.car_count = bpy.context.scene.car_count
        return self.execute(context)
        
        
class GetCameraVertices(bpy.types.Operator):
    bl_idname = "mesh.get_camera_vertices"
    bl_label = "Get Vertex Coordinates of the Active Object on Camera Viewport"
    bl_options = {"UNDO"}
    
    def execute(self, context):
        if self.scene is None:
            print("Error: No scene!")
            return {"CANCELLED"}
        if  bpy.context.scene.camera is None:
            print("Error: No camera!")
            return {"CANCELLED"}
        scene = bpy.context.scene
        render = scene.render
        res_x = render.resolution_x
        res_y = render.resolution_y
        
        obj = bpy.context.active_object
        cam = bpy.context.scene.camera#self.scene.camera
        
        verts = (vert.co for vert in obj.data.vertices)
        coords_2d = [bpy_extras.object_utils.world_to_camera_view(scene, cam, coord) for coord in verts]
        
        rnd = lambda i: round(i)
        
        print('x,y')
        for x,y, distance_to_lens in coords_2d:
            print("{},{}".format(rnd(res_x*x), rnd(res_y*y)))
        print(" ")
        return {"FINISHED"}
        
    def invoke(self, context, event):
        self.scene = global_data["city_scene"]
        return self.execute(context)
    
    
class CityScenePanel(bpy.types.Panel) :
    bl_space_type = "VIEW_3D"
    bl_region_type = "TOOLS"
    bl_context = "objectmode"
    bl_category = "Create"
    bl_label = "(Re)create City Scene"

    def draw(self, context) :
        the_col = self.layout.column(align = True)
        the_col.prop(context.scene, "grid_size")        
        the_col.prop(context.scene, "street_count")
        the_col.prop(context.scene, "car_count")
        the_col.operator("mesh.create_city_scene", text = "(Re)create City Scene")
        the_col.operator("mesh.create_camera_path", text = "(Re)generate Camera Path")
        the_col.operator("mesh.render_city_scene", text = "Render Scene along path")
        row = self.layout.row(align = True)
        
class CameraVerticesPanel(bpy.types.Panel):
    bl_space_type = "VIEW_3D"
    bl_region_type = "TOOLS"
    bl_context = "objectmode"
    bl_category = "Create"
    bl_label = "Get Vertex Coordinates on Camera"
    
    def draw(self, context):
        this_col = self.layout.column(align = True)
        this_col.operator("mesh.get_camera_vertices", text = "Get Vertex Coordinates on Camera Viewport")
        row2 = self.layout.row(align = True)


CUSTOM_CLASSES = [
    CreateCityScene,
    CreateCameraPath,
    RenderCityScene,
    GetCameraVertices,
    CityScenePanel,
    CameraVerticesPanel
]

CUSTOM_PROPS = {
    "grid_size" : bpy.props.IntProperty(
        name = "Grid size",
        description = "Size of the whole grid",
        default = 20
    ),  
    "street_count" : bpy.props.IntProperty( 
        name = "# Streets",
        description = "Number of street paths generated",
        default = 5
    ),
    "car_count" : bpy.props.IntProperty(
        name = "# Cars",
        description = "Number of cars driving through the city at any time",
        default = 20
    )
}
        

