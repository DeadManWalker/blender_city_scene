import numpy as np
import itertools
import random


class OredTuple(tuple):
    """
    Tuple subclass that supports the logical or operator
    
    Exp.: OredTuple((1,2,3)) | 4 => OredTuple((1|4, 2|4, 3|4))
    Exp.: OredTuple((1,2)) | (4,5) => OredTuple((1|4, 1|5, 2|4, 2|5))        
    """
    
    def __new__(cls, *args):
        if args and hasattr(args[0], "__iter__"):
            args = args[0]
        return super().__new__(cls, args)
    
    def __or__(self, other):
        if not hasattr(other, "__iter__"):
            other = (other, )
        return OredTuple(*tuple(map(np.bitwise_or.reduce, itertools.product(self, other))))
    
    def __ror__(self, other):
        return self | other




class TurnFilterCreator:
    """ Creates turn filter functions to manipulate the turn probabilities
    for CityScene.addStreetPath """
    
    @staticmethod
    def staticProbabilities(left, straight, right):
        """ Return the given, static probabilities """
        l_s_r = (left, straight, right)
        l_s_r = np.divide(l_s_r, sum(l_s_r))
        def tfilter(city_scene, path_data, turn_probs, dir_probs):
            turn_probs[city_scene.TURN_LEFT] = l_s_r[0]
            turn_probs[city_scene.TURN_STRAIGHT] = l_s_r[1]
            turn_probs[city_scene.TURN_RIGHT] = l_s_r[2]
            return {"turn_probs" : turn_probs}
        return tfilter             

    @staticmethod
    def balanceTurns(factor=1):
        """ Balances left and right turn out by penalizing/ favoring the turn probabilities
        by the turn overlap times a factor """
        def tfilter(city_scene, path_data, turn_probs, dir_probs):
            turn_avg = sum(path_data["turns"])
            if turn_avg:
                turn_idx_penalize, turn_idx_favor = np.sign(turn_avg), -np.sign(turn_avg)
                adjustment = turn_probs[turn_idx_penalize] - turn_probs[turn_idx_penalize] / ((abs(turn_avg)+1)*factor)
                turn_probs[turn_idx_penalize] -= adjustment
                turn_probs[turn_idx_favor] += adjustment
            return {"turn_probs" : turn_probs}
        return tfilter             
            
    @staticmethod
    def noConsecutiveTurns():
        """ Shift the probability of the last turn (if any) to the opposite one """
        def tfilter(city_scene, path_data, turn_probs, dir_probs):
            last_turn = path_data["turns"][-1]
            if last_turn:
                # Set probability of this turn to zero and add it to the opposite turn
                last_turn_prob = turn_probs[last_turn]
                turn_probs[last_turn] = 0
                turn_probs[-last_turn] += last_turn_prob
            return {"turn_probs" : turn_probs}
        return tfilter

    @staticmethod
    def noTurnsAfterIntersection(): 
        """ Force a straight move after an intersection """       
        TWO_WAY_FLAGS = [2**n for n in range(1,7)]
        def tfilter(city_scene, path_data, turn_probs, dir_probs):
            if len(path_data["indices"]) > 1:
                last_point = path_data["indices"][-2]
                if city_scene.grid_flags[last_point[1]][last_point[0]] not in TWO_WAY_FLAGS:
                    turn_probs[city_scene.TURN_LEFT] = 0
                    turn_probs[city_scene.TURN_RIGHT] = 0
                    turn_probs[city_scene.TURN_STRAIGHT] = 1
            return {"turn_probs" : turn_probs}
        return tfilter

    @staticmethod
    def noParallelPaths(): 
        """ Avoid parallel paths by forcing the direction towards the a neighboring
        path """ 
        def tfilter(city_scene, path_data, turn_probs, dir_probs):
            current = path_data["indices"][-1]
            for direction in dir_probs.keys():
                turn_point = np.add(current, direction)
                # Find and set the direction towards a neighboring path if there is one
                if city_scene.inBounds(turn_point) and \
                 city_scene.grid_flags[turn_point[1]][turn_point[0]] > 0:
                    # Force direction probability to the one facing a neighboring path
                    dir_probs = {dir : 0 for dir in dir_probs.keys()} 
                    dir_probs[direction] = 1
                    break
            return {"direction_probs" : dir_probs}
        return tfilter

    @staticmethod
    def intersectOrJoin(join_probability=0.5): 
        """ If paths cross each other, decide whether to intersect or join the other
        path. Decision to intersect may be overridden with decision to join if the
        path across is not clear """     
        TWO_WAY_FLAGS = [2**n for n in range(1,7)]
        def tfilter(city_scene, path_data, turn_probs, dir_probs):
            current = path_data["indices"][-1]
            if city_scene.grid_flags[current[1]][current[0]] not in TWO_WAY_FLAGS:
                return
            stop = False
            # Choose path intersection or joint
            join = random.random() < join_probability
            if not join: # Intersect paths to a 4-way
                dir_probs = {dir : 0 for dir in dir_probs.keys()} # Reset direction probabilities to 0   
                # Find and set the direction to the still empty grid point if it exists
                for direction in dir_probs.keys():
                    turn_point = np.add(current, direction)
                    neighbors = ( np.array(list( city_scene.getNeighbors(turn_point, out_of_bounds_as=1) )) > 0 ).sum()
                    if city_scene.inBounds(turn_point) and city_scene.grid_flags[turn_point[1]][turn_point[0]] == 0 \
                     and neighbors < 4:
                        # Take direction to the empty point found and force it
                        dir_probs[direction] = 1
                        break
                # No empty grid point for a valid intersection, so join
                else:
                    join = True             
            if join: # Join paths to a 3-way
                dir_probs = {dir : 0 for dir in dir_probs.keys()} # Reset direction probabilities to 0   
                stop = True # Stop the path generation since we now joined another path
                # Find and set the final direction along one direction of the path to be joined
                for direction in dir_probs.keys():
                    turn_point = np.add(current, direction)
                    if city_scene.inBounds(turn_point) and city_scene.grid_flags[turn_point[1]][turn_point[0]] > 0:
                        # Take the first direction found and force it
                        dir_probs[direction] = 1
                        break
            return {"direction_probs" : dir_probs, "stop" : stop}
        return tfilter

