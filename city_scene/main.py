import bpy
from bpy.props import BoolProperty

# Reloading of all submodules necessary
from importlib import reload
import city_scene_script.bpy_objects
import city_scene_script.city_scene
import city_scene_script.helper
reload(city_scene_script.bpy_objects)
reload(city_scene_script.city_scene)
reload(city_scene_script.helper)

from city_scene_script.bpy_objects import *

#
# TODOS/ BUGS:
# - UDL-texture sometimes flipped
# - Add grass tiles to smaller buildings
# - Paths to keyframes -> partially done
# - collision checking



def register() :
    for cclass in CUSTOM_CLASSES:
        bpy.utils.register_class(cclass)

    for cprop_name, cprop_type in CUSTOM_PROPS.items():
        setattr(bpy.types.Scene, cprop_name, cprop_type)
        
    
def unregister() :
    for cclass in CUSTOM_CLASSES:
        try:
            bpy.utils.unregister_class(cclass)
        except RuntimeError:
            pass

    for cprop_name, cprop_type in CUSTOM_PROPS.items():
        try:
            delattr(bpy.types.Scene, cprop_name)
        except AttributeError:
            pass

if __name__ == "__main__" :
    register()

