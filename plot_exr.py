#!/usr/bin/python3

'''
Based off: http://www.tobias-weis.de/groundtruth-data-for-computer-vision-with-blender/
'''

import OpenEXR
import Imath
import array
import math
import numpy as np
import cv2
import matplotlib
import matplotlib.pyplot as plt
import os
import struct

matplotlib.use('TkAgg')


BASE_PATH = "/home/dmw/projects/lightfield_data/sequence"


class FilenameWalker(object):
    def __init__(self, base_path):
        self.base_path = os.path.abspath(os.path.expanduser(base_path))
        self.filenames = os.listdir(self.base_path)
        
    def walkPrefix(self, prefix, return_format="full"):
        if return_format == "short":
            ret_func = lambda x: x
        elif return_format == "full":
            ret_func = lambda x: os.path.join(self.base_path, x)
        elif return_format == "separate":
            ret_func = lambda x: (self.base_path, x)
        files = sorted(ret_func(file) for file in self.filenames if file.startswith(prefix))
        for file in files:
            yield(file)


class Figure(object):
    def __init__(self):
        self.fig = plt.figure()
        self.index = 0
        self.handler_id = None
        self.series = []
        self.titles = []

    def showImages(self, titles, images):
        rows, cols = 2, math.ceil(len(images)/2.0)
        for i, (title, img) in enumerate(zip(titles, images), 1):
            self.fig.add_subplot(cols, rows, i)
            plt.title(title)
            plt.imshow(img)
        self.fig.canvas.draw()

    def onClick(self, event):
        if event.key == "right":
            self.index += 1
            if self.index >= len(self.series):
                self.index = 0
            self.showImages(self.titles, self.series[self.index])
        elif event.key == "left":
            self.index -= 1
            if self.index < 0:
                self.index = len(self.series) - 1
            self.showImages(self.titles, self.series[self.index])


    def showImagesSeries(self, titles, series):
        if self.handler_id is not None:
            self.fig.canvas.mpl_disconnect(handler_id)
        self.handler_id = self.fig.canvas.mpl_connect('key_press_event', self.onClick)
        self.titles = titles
        self.series = list(zip(*series))
        self.showImages(self.titles, self.series[self.index])

    def show(self):
        plt.colorbar() 
        plt.show()


def exrToNpImg(exr, maxvalue=0, normalize=True):
    """ converts 1-channel exr-data to 2D numpy arrays """                                                       
    file = OpenEXR.InputFile(exr)

    # Compute the size
    dw = file.header()['dataWindow']
    size = (dw.max.x - dw.min.x + 1, dw.max.y - dw.min.y + 1)

    # Read the three color channels as 32-bit floats
    FLOAT = Imath.PixelType(Imath.PixelType.FLOAT)
    R, G, B = [ np.array(array.array('f', file.channel(chan, FLOAT)).tolist()) for chan in ("R", "G", "B") ]

    # create numpy 2D-array
    img = np.zeros((size[1], size[0], 3), np.float64)

    img[:,:,0] = np.array(R).reshape(img.shape[0], -1)
    img[:,:,1] = np.array(G).reshape(img.shape[0], -1)
    img[:,:,2] = np.array(B).reshape(img.shape[0], -1)

    # normalize
    if maxvalue > 0:
        img[img > maxvalue] = maxvalue
    if normalize:
        img /= np.nanmax(img)
    return img


def npImgToFlow(img): 
    # R channel contains x-offset
    # G channel contains y-offset
    img[:,:,1] *= -1 # Blender y axis points upwards

    mag, ang = cv2.cartToPolar(img[...,0], img[...,1])

    hsv = np.zeros(img.shape, np.uint8)
    hsv[...,1] = 255
    hsv[...,0] = ang*180/np.pi/2
    hsv[...,2] = cv2.normalize(mag, None, 0, 255, cv2.NORM_MINMAX)
    bgr = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)

    return bgr, mag, ang


def flowToSintelFormat(offset_x, offset_y, filename):
    TAG_STRING = 'PIEH' # use this when WRITING the file
    assert(offset_x.shape == offset_y.shape)
    height, width = offset_x.shape

    pack_string = "<{}sII".format(len(TAG_STRING))
    header = struct.pack(pack_string, TAG_STRING.encode(), width, height)

    with open(filename, "wb") as file:
        file.write(header)
        for x, y in zip(offset_x.flat, offset_y.flat):
            file.write(struct.pack("<ff", x, y))


def exrToSintelFormat(exr, maxvalue=0, normalize=True):
    img = exrToNpImg(exr, maxvalue, normalize)
    offset_x, offset_y = img[:,:,0], img[:,:,1]
    filename = exr.rsplit(".", 1)[0] + ".flo"
    flowToSintelFormat(offset_x, offset_y, filename)
    
   

def main(): 
    walker = FilenameWalker(BASE_PATH)

    def getImgsImage(prefix="image"):        
        return (exrToNpImg(path) for path in walker.walkPrefix(prefix))

    def getImgsDepth(prefix="depth"):
        return (exrToNpImg(path)[:,:,0] for path in walker.walkPrefix(prefix))

    def getImgsNormals(prefix="normals"):        
        return (exrToNpImg(path) for path in walker.walkPrefix(prefix))

    def getImgsFlow(prefix="speed"):
        offsets_x, offsets_y, bgrs, mags, angs = [], [], [], [], []
        for path in walker.walkPrefix(prefix):
            img = exrToNpImg(path)
            offset_x, offset_y = img[:,:,0], img[:,:,1]
            offsets_x.append(offset_x)
            offsets_y.append(offset_y)
            bgr, mag, ang = npImgToFlow(img)
            bgrs.append(bgr)
            mags.append(mag)
            angs.append(ang)
        return (offsets_x, offsets_y, bgrs, mags, angs)


    """
    titles = ("Image", "Depth", "Normals", "Offset X", "Offset Y", "BGR", "Magnitude", "Angle")
    imgs_series = (getImgsImage(), getImgsDepth(), getImgsNormals(), *getImgsFlow())
    
    figure = Figure()    
    figure.showImagesSeries(titles, imgs_series)
    figure.show()
    """

    """
    titles = ("Flow_bw", "Flow_fw")
    imgs_series = (getImgsImage("Flow_bw_frame"), getImgsImage("Flow_fw_frame"))
    figure = Figure()    
    figure.showImagesSeries(titles, imgs_series)
    figure.show()
    """

    for filename in walker.walkPrefix("Flow_", return_format="full"):
        print("Reading, converting and outputting %s as Sintel flow format" %filename)
        exrToSintelFormat(filename)


if __name__ == "__main__":
    main()


