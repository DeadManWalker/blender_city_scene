function showFlowFiles(file_structs, base="")


file_paths = arrayfun(@(f){fullfile(base, f.name)}, file_structs);
n = length(file_paths);

color_imgs = {}
for i = 1:n
    color_imgs{end+1} = flowToColor(readFlowFile(file_paths{i}));
end

width = ceil(sqrt(n));
height = ceil(n/width);

f = figure(1);
clf;
#f.NextPlot = 'new';
title("Optical Flow");
for counter = 1:n
    subplot(width, height, counter, "Parent", f);
    imshow(color_imgs{counter});
    if(counter == n)
    	colorbar();
    endif
end
