\documentclass[conference]{IEEEtran}
%\documentclass{article}
\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnote. If that is unneeded, please comment it out.
\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\usepackage{hyperref}
\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}
\begin{document}

\title{Randomly Generated City Scene to train Autonomous Driving}


\maketitle

\begin{abstract}
This is the report for the internship at the Heidelberg Collaboratory for Image Processing (HCI)\footnote{\url{https://hci.iwr.uni-heidelberg.de}} where we created a randomly generated city scene with buildings, roads and cars driving on those roads as a Blender\footnote{Blender is an open-source 3D program. \url{https://www.blender.org/}} plugin.
\end{abstract}

\section{Introduction}
Autonomous driving\cite{autodriving_1}\cite{autodriving_2} still sounds like wild dreams from the future, but it is closer than it has ever been before. However, to test a newly crafted autonomous driving system, it usually is expensive and dangerous to do so in the real world, so other ways of testing the system are needed. Nothing is worse than having an accident involving injuries because your algorithm is flawed\cite{carcrash}. Therefore we created the Random City Scene Generator, a plugin for the largest open-source 3D graphics software that is available and used in lots of commercial and private places. Using our plugin, users can generate a dynamically sized city scene that is randomly generated, making it extremely helpful to test and train autonomous driving systems on since the dataset changes every time and the data is generated easily and harmlessly.

\section{What is needed for Autonomous Driving?}
A self driving car has lots of input information to be able to move around and to avoid any misbehavior like taking the wrong route or crashing into objects. They have a variety of sensors installed which all offer different ways to perceive the real world such as GPS, odometry, sonar, lidar and radar\cite{autodriving_2}. With those input signals the autonomous system is able to create a virtual world which should ideally render the real world as good as possible and the system should be able to navigate its way through it. Those sensors each transfer different information about the surroundings, e.g. the lidar is used to measure the distance to a target by measuring the light coming from a laser and getting reflected by the target, the sonar uses sound propagation to detect objects by emitting sounds and detecting the responding waves or passively listening to the emitted sound waves from the surrounding area.
\\In our case, the hypothetical self driving car only uses a camera to perceive its surroundings which is the most human-based approach since we use our eyes most of the time to see the road ahead and everything around it, only lacking the acoustic input that is used to hear other cars and objects emitting sounds that are important to the driver. 

\section{Creating the  City Scene}
Hereinafter the process of generating our city scene is described, from small random paths\cite{randomwalk} to cars driving on the streets next to randomly generated buildings.
\subsection{The Generation of a Street}
To create the digital driving playground for our testing car, the first thing it needs is a street. If we look at road maps of cities, most of the time streets are arranged in a grid-like pattern where the turns tend to be in a ninety degree shape and crossings also have their respective outgoing streets in an orthogonal arrangement. So the easiest and most efficient way of creating the street layout was to use tiles in a square shape where each tile represents an important part of the street like ninety degree turns, three- or four-way crossings and straight roads in horizontal and vertical direction. And of course, since they are tiles, they connect to each other seamlessly if the edges match by having an outgoing road at the connecting edge.
\\Having all tiles ready, we now needed a street layout to fill with those tiles. Here we use a simple random path algorithm to generate multiple random paths on a regular grid with dynamic size to alter the size of the final street map. The number of paths can be dynamically set in the Blender toolbar. Using the information from the generated paths, we can fill the grid with tiles by storing the path direction in each grid point assuming that the paths start going orthogonal into the grid and out of it and don't start at corners. Since we have multiple paths which are very likely to intersect or overlap, we add each path to the grid and check for points where there is already a tile information and use the bitwise OR\footnote{Python documentation: \url{https://docs.python.org/2/reference/datamodel.html\#object.__or__}} operator to distinguish between different directions but discard the same direction and in the end check for all listed directions for this point and put the respective tile there. In the following Figure \ref{fig:crossingpaths} a simplified version of this mechanic can be seen. The first path (seen in green) goes vertically\footnote{The direction is actually not important since streets are usually both ways (except one-way streets which are not modeled in this project)} and causes the grid points to save the direction "down" as their initial direction for the later street tiling. Now a second path (seen in orange) comes in horizontally and crosses the points from the first path at the center point which now stores both directions "right" and "down". This causes the tiling process to place a four-way crossing since the lookup dictionary has the key "down|right" mapped to the value four-way crossing which is mapped to the respective tile. The remaining tiles get the respective straight street tile to complete the street grid.\begin{figure}
	\includegraphics[width=\linewidth]{figures/path_intersection}
	\caption{Two paths creating a four-way crossing}
	\label{fig:crossingpaths}
\end{figure}
Figure \ref{fig:streetpath} shows an example of the result from our random path generation and tiling with a grass texture as a background behind the tiles.
\begin{figure}
	\includegraphics[width=\linewidth]{figures/street_paths}
	\caption{An example of our randomly generated street grid}
	\label{fig:streetpath}
\end{figure}

\subsection{Building the Buildings}
Now that the street grid is set up, we need to extend the city scene to further match a real city. So the most important thing missing are buildings. To get a diverse look, we created six hand-crafted and varying building models with all having a square platform with the same maximum width to place them similar to the street tiles. Since buildings are usually directly next to a street we made sure that our scene follows this behavior. \\The remaining grid points without a street tile are taken as possible places to put a building onto it. Points adjacent to a street are given a much higher likelihood to have a building at their position and now we simply let the probability decide where to place buildings. The buildings also get randomly rotated by a multiple of ninety degree to get a more diverse scenery. Figure \ref{fig:buildings} shows the street scene now with buildings placed randomly. It can be seen that buildings tend to be placed next to a street just like in real cities.
\begin{figure}
	\includegraphics[width=\linewidth]{figures/buildings}
	\caption{The previously shown street grid now with randomly placed buildings}
	\label{fig:buildings}
\end{figure}

\subsection{Letting the Cars drive}
Now that the city itself is standing it is time to add some cars. In Blender you can create animations very easily by keyframing\footnote{Keyframing explained in Blender: \url{https://docs.blender.org/manual/en/latest/animation/keyframes/index.html}} the change in an object's properties, e.g. to create a movement you keyframe the location for different time steps or you can keyframe the size, rotation, material; almost anything can be keyframed. To make the cars we received from this 3D model website\footnote{\url{https://chocofur.com/}} drive on our streets, we only need to keyframe their location on the street paths and their rotation when passing a turn or crossing. The paths already existed when creating the streets itself so we can simply use this data for our cars now. We randomly choose street paths for a set amount of cars and check for time-dependent intersections of those paths so cars don't go through each other. To get a realistic animation we also make the cars rotate towards the bend on tiles where the path has a corner. If a car reaches the end of the map, it simply disappears to avoid clutter and to have the scene loopable. Figure \ref{fig:cars} shows the cars in the scene and how they rotate on specific tiles where the path has a corner.
\begin{figure}
	\includegraphics[width=\linewidth]{figures/cars}
	\caption{Cars are now driving in the scene and rotate while moving according to the tiles}
	\label{fig:cars}
\end{figure}

\subsection{Final Steps}
To simulate an actual driving car, we now only need to replace one of the cars with a camera. The camera also takes over all previously created keyframes to make the it behave like a car. If we were to render the animation now it should look similar to a camera positioned inside a real car driving on the streets of a city. 

\section{Basics for Autonomous Driving}
\subsection{Edge Detection for Object Recognition}
To begin an autonomous driving system one of the first steps is to be able to detect objects\cite{objrec}. Most of the common algorithms that take images as input use an edge detection algorithm to extract feature points from the image because a computer can't do much with an image on its own. If the edges of an image are extracted, the algorithm can start building a database by learning from given images with a known label or take an already learned one from other sources. Using the database, an algorithm now can use matching to find the best-fitting object in an image by comparing it to the database entries and output the object it found. With this technique, autonomous driving may be achieved from a normal camera by recognizing the objects in front of the car and by classifying them into a range of actions to find out for example what action to do when a human walks in front of the car or a traffic light is ahead and showing a red light. Of course, for the latter we would also need to pay attention to the color of the traffic light object.
\subsection{Corner Detection}
To get an easy testing ground for the already created autonomous driving algorithms we implemented a function to output an object's corners with their respective coordinates on the camera image. With this information the algorithm can calculate the edges of an object or directly classify based on the corners since those are sufficient for object detection as well, and it can be tested whether the program detected the correct object at the right place. So we built the ground truth data for other algorithms to do testing on.

\section{Further Work}
To continue on the project, we will try to make the scene more photorealistic since the realism of a scene is what makes the testing ground better, because the more it matches the real world the better algorithms will be that are trained on this testing project. For example most of the important street objects are still missing like traffic lights, crosswalks and signs, and an autonomous driving system needs to be able to recognize those objects correctly to be a complete member of street traffic. To guarantee a secure driving system, a simulation of humans is also necessary. However, this would exceed the extent of an internship to model and animate human beings on a realistic level and simulate human behavior. Furthermore, you could create random events to prepare the system for unexpected occurrences like people suddenly crossing the street or not paying attention at the traffic rules. 
\\To conclude, our project acts as a good starting point for the testing of autonomous driving systems, but to actually get them to be able to participate in real traffic, way more input and training is needed to make the algorithms aware of any event that might result in damage to objects or people.

\newpage
\begin{thebibliography}{00}
	\bibitem{autodriving_1} Todd Lassa \textit{The Beginning of the End of Driving}. Motor Trend, 2014 \url{https://www.motortrend.com/news/the-beginning-of-the-end-of-driving/}
	\bibitem{autodriving_2} Dr. Jadranka Dokic, Dr. Beate Müller, Dr. Gereon Meyer \textit{European Roadmap: Smart Systems for Automated Driving}. 2015 \url{https://web.archive.org/web/20150212024339/http://www.smart-systems-integration.org/public/documents/publications/EPoSS%20Roadmap_Smart%20Systems%20for%20Automated%20Driving_2015_V1.pdf}
	\bibitem{carcrash} Josh Delk \textit{Video released by police shows view from inside self-driving car before fatal crash}. The Hill, 2018 \url{https://thehill.com/policy/technology/379656-video-released-by-police-shows-view-from-inside-self-driving-car-before}
	\bibitem{objref} Ahmed Elgammal \textit{3D Model-based recognition}. Rutgers University, 2010 \url{https://www.cs.rutgers.edu/\~{}elgammal/classes/cs534/lectures/3D\_modelbasedvision.pdf}
	\bibitem{randomwalk} David Aldous, James Allen Fill \textit{Reversible Markov Chains and Random Walks on Graphs}. Archieved from original on 2019 \url{https://www.stat.berkeley.edu/~aldous/RWG/book.html}
	\bibitem{objrec} Jason Brownlee \textit{A Gentle Introduction to Object Recognition With Deep Learning}. Machine Learning Mastery, 2019 \url{https://machinelearningmastery.com/object-recognition-with-deep-learning/}
\end{thebibliography}


\end{document}