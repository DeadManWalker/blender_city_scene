\documentclass{beamer}

\usepackage[utf8]{inputenc}
\usepackage[autostyle]{csquotes}
\usepackage{fancybox}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{tabulary}
\usepackage{tabularx}
\usepackage[export]{adjustbox}
\usepackage[style=numeric]{biblatex}

\addbibresource{library.bib}
 
 
\title{An Introduction to Optical Flow and Light Fields using Blender}
\author{Kevin Hambrecht}
\date{10.03.2020}

\usetheme{metropolis}
\usecolortheme{default}

\begin{document}
 
\frame{\titlepage}

\section{An Introduction}

\subsection{Motivation}
\begin{frame}
	\frametitle{Motivation}
	\begin{itemize}
		\item \textbf{Machine learning algorithms} need huge amounts of training data
		\item \textbf{Training data} consists of application-specific ground truth data
		\item Real-world \textbf{Ground truth data} is usually expensive
		\item \textbf{Synthetic data} can be cheap due to software automation
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Motivation: Types of Data}
	Typical types of data:
	\begin{columns}
		\column{0.45\textwidth}
			\begin{itemize}
			\item Depth map
			\item Disparity map
			\item Segmentation (+ semantic annotation)
			\item Illumination map
			\item Normal map
			\item Optical flow
			\item Light field
			\item ...
		\end{itemize}
		\column{0.55\textwidth}
		\begin{figure}[h]
			\center
			\includegraphics[width=1\textwidth]{assets/ground_truth_example}
			\caption{TL: Source image, TR: Depth map, BL: Optical Flow, BR: Segmentation\cite{gt-example}}
		\end{figure}
	\end{columns}
\end{frame}

\begin{frame}
	\frametitle{Motivation: Blender as a Framework}
	Why choose Blender as a Framework?
	\begin{figure}[h]
		\center
		\includegraphics[width=0.5\textwidth]{assets/blender}
		\caption{Blender on start-up}
	\end{figure}
	\begin{itemize}
		\item Free, open-source 3D computer graphics software
		\item Applications: modeling, animation, simulation, rendering, compositing, ...
		\item Extensive API through embedded Python interpreter
	\end{itemize}
\end{frame}

\subsection{Outline}
\begin{frame}
\frametitle{Outline}
	\begin{enumerate}
		\item Optical Flow
		\item Light Field
		\item Apply Findings in Blender
		\item Work Findings into Blender Addon Tool
		\item Conclusion
	\end{enumerate}
\end{frame}


\section{Optical Flow}
\begin{frame}
\frametitle{Optical Flow}
	\begin{itemize}
		\item Discrete vector field
		\item Pixel position displacement of two (neighboring) image frames
		\item Motion of scene, relative to observer
	\end{itemize}
	\begin{figure}
		\centering
		\begin{subfigure}{.3\textwidth}
			\center
			\includegraphics[width=\textwidth]{assets/cube_flow_field3}
			\caption{Two frames of rightwards moving cube}
		\end{subfigure}
		\begin{subfigure}{.3\textwidth}
			\center
			\includegraphics[width=\textwidth]{assets/cube_flow_field2}
			\caption{Optical flow of rightwards moving cube (overlayed)}
		\end{subfigure}
		\begin{subfigure}{.3\textwidth}
			\center
			\includegraphics[width=\textwidth]{assets/cube_flow_field1}
			\caption{Optical flow of rightwards moving cube}
		\end{subfigure}
	\end{figure}
\end{frame}

\begin{frame}
\frametitle{Optical Flow}
	\begin{itemize}
		\item Capture optical flow during recording/ creation of data
		\item Reconstruct optical flow by estimation method (e.g. Detect and track)
	\end{itemize}
	\begin{itemize}
		\item Store sparse vector field
		\item Store dense vector field
	\end{itemize}
\end{frame}


\section{Light Field}
\begin{frame}
\frametitle{Light Field}
	\begin{itemize}
		\item Vector field of (all) light rays
		\item Ray information: Position (3D coordinates) + direction (two angles) = 5D plenoptic function
		\item Passing through an observer, one dimension is eliminated (assuming constant radiance) $\Rightarrow$ 4D light field
	\end{itemize}
	\begin{figure}[h]
		\center
		\includegraphics[width=0.3\textwidth]{assets/plenoptic_function}
		\caption{5D Plenoptic Function\cite{plenoptic}}
	\end{figure}
\end{frame}

\begin{frame}
\frametitle{Light Field: Capture Data}
	\begin{itemize}
		\item Capture many sub-images of one scene, offset by small amount
		\item Each sub-image has slightly different perspective
		\item Disparity between sub-image pixel
		\item Calculate color and angularity of captured light rays
	\end{itemize}
	\begin{figure}[h]
		\center
		\includegraphics[width=0.35\textwidth]{assets/lf_subimages}
		\caption{Sub-images of one scene using three constantly offset cameras\cite{lf-forum}}
	\end{figure}
\end{frame}

\begin{frame}
\frametitle{Light Field: Setups}
	\begin{itemize}
		\item single, robotically controlled camera
		\item rotating arc of cameras
		\item array of cameras/ camera modules
		\item single light field/ plenoptic camera composed of microlens array
		\end{itemize}
	\begin{figure}
		\centering
		\begin{subfigure}{.3\textwidth}
			\center
			\includegraphics[width=\textwidth]{assets/lightfield_camera_array}
			\caption{Camera array\cite{lf-forum}}
		\end{subfigure}
		\begin{subfigure}{.3\textwidth}
			\center
			\includegraphics[width=\textwidth]{assets/lf_robot_camera}
			\caption{Robotically controlled camera\cite{lf-forum}}
		\end{subfigure}
		\begin{subfigure}{.3\textwidth}
			\center
			\includegraphics[width=\textwidth]{assets/lf_lytro_camera}
			\caption{Lytro camera (microlens array)\cite{lf-forum}}
		\end{subfigure}
	\end{figure}
\end{frame}

\begin{frame}
\frametitle{Light Field: Applications}
	\begin{itemize}
		\item 3D reconstruction of scene/ depth reconstruction
		\item Light field rendering (2D slice from 4D LF)
		\item Post adjusting depth of field, focus, perspective shift
		\item Glare reduction
	\end{itemize}
	\begin{figure}[h]
		\center
		\includegraphics[width=0.5\textwidth]{assets/focus_change}
		\caption{Focus change from background to butterfly\cite{lf-forum}}
	\end{figure}
\end{frame}


\section{Apply Findings in Blender}
\begin{frame}
\frametitle{Apply Findings in Blender}
	\begin{center}
		\huge{Goal:}
	\end{center}
	\begin{center}
		Set up test scene and extract \textbf{depth}, \textbf{normals}, \textbf{optical flow} and \textbf{light field} data
	\end{center}
\end{frame}

\begin{frame}
\frametitle{Apply Findings in Blender: Test Scene}
	\begin{figure}[h]
		\center
		\includegraphics[width=\textwidth]{assets/blender_exporting_scene}
		\caption{Blender scene for exporting ground truth data}
	\end{figure}
\end{frame}

\begin{frame}
\frametitle{Apply Findings in Blender: Exported Data}
	\begin{figure}
		\centering
		\begin{subfigure}{.49\textwidth}
			\center
			\includegraphics[width=\textwidth]{assets/cubes_image}
			\caption{Rendered cubes images}
		\end{subfigure}
		\begin{subfigure}{.49\textwidth}
			\center
			\includegraphics[width=\textwidth]{assets/cubes_depth}
			\caption{Visualized exported depth}
		\end{subfigure}
	\end{figure}
\end{frame}

\begin{frame}
\frametitle{Apply Findings in Blender: Exported Data}
	\begin{figure}
		\centering
		\begin{subfigure}{.49\textwidth}
			\center
			\includegraphics[width=\textwidth]{assets/cubes_normal}
			\caption{Visualized exported normals}
		\end{subfigure}
		\begin{subfigure}{.49\textwidth}
			\center
			\includegraphics[width=\textwidth]{assets/cubes_flow}
			\caption{Visualized exported optical flow in MPI Sintel format}
		\end{subfigure}
	\end{figure}
\end{frame}

\begin{frame}
\frametitle{Apply Findings in Blender: Light Field Data}
	\begin{itemize}
		\item Simulated by variable sized grid of camera
		\item Operation within the cameras view frustum constrained by two planes
		\item Setting camera parameters: focal length, x+y resolution, sensor chip size, ...
		\item Render scene from each camera (sub-image)
	\end{itemize}
\end{frame}


\section{Work Findings into Blender Addon Tool}

\begin{frame}
\frametitle{Work Findings into Blender Addon Tool}
	\begin{center}
		\huge{Goal:}
	\end{center}
	\begin{center}
		Create an \textbf{automated} process for generating \textbf{random scenes} and exporting its \textbf{ground truth data}
	\end{center}
	\begin{center}
		\LARGE{More precise:}
	\end{center}
	\begin{center}
		A \textbf{Blender addon} for generating random, tile-based \textbf{city scenes} consisting of \textbf{buildings}, \textbf{streets} and \textbf{moving cars}.
		Exporting the \textbf{ground truth} data while \textbf{moving the renderer along a street path} in the scene.
	\end{center}
\end{frame}

\begin{frame}
\frametitle{Work Findings into Blender Addon Tool: A Scene}
	\begin{figure}[h]
		\center
		\includegraphics[width=0.95\textwidth]{assets/blender_addon_scene}
		\caption{Randomly generated Blender city scene. Pink arrows indicate renderer path.}
	\end{figure}
\end{frame}


\section{Conclusion}
\begin{frame}
\frametitle{Conclusion}
	\begin{itemize}
		\item Optical flow and light fields contain geometric and structural information
		\item Need for fast and inexpensive ground truth data is rising
		\item Automatically generated, synthetic and broad ground truth data as shown will accelerate the research
		\item More (photo) realistic scenes may be needed to train good models
		\item Possibly build on top of existing digital scenes with semantic and ground truth access such as games
	\end{itemize}
\end{frame}




\begin{frame}
	\alert{\Huge{\textbf{Questions?}}}
\end{frame}

\begin{frame}[allowframebreaks]
	\printbibliography
\end{frame}
 
\end{document}


